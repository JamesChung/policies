import boto3
import json
from botocore.exceptions import ClientError


s3 = boto3.client("s3")
BUCKET_NAME = "testing-bukkit"
OTHER_BUCKET_NAME = "other-test-bukkit"

def main():
    test_copy()
    test_copy_object()
    test_create_multipart_upload()
    test_delete_bucket()
    test_delete_bucket_analytics_configuration()
    test_delete_bucket_cors()
    test_delete_bucket_encryption()
    test_delete_bucket_inventory_configuration()
    test_delete_bucket_lifecycle()
    test_delete_bucket_policy()
    test_delete_bucket_replication()
    test_delete_bucket_tagging()
    test_delete_bucket_website()
    test_delete_object()
    test_delete_object_tagging()
    test_delete_objects()
    test_delete_public_access_block()
    test_download_file()
    test_download_fileobj()
    test_get_bucket_accelerate_configuration()
    test_get_bucket_acl()
    test_get_bucket_cors()
    test_get_bucket_encryption()
    test_get_bucket_inventory_configuration()
    test_get_bucket_lifecycle()
    test_get_bucket_location()
    test_get_bucket_logging()
    test_get_bucket_metrics_configuration()
    test_get_bucket_notification()
    test_get_bucket_notification_configuration()
    test_get_bucket_policy()
    test_get_bucket_policy_status()
    test_get_bucket_replication()
    test_get_bucket_request_payment()
    test_get_bucket_tagging()
    test_get_bucket_versioning()
    test_get_bucket_website()
    test_get_object()
    test_get_object_acl()
    test_get_object_lock_configuration()
    test_get_object_tagging()
    test_get_object_torrent()
    test_get_public_access_block()
    test_head_bucket()
    test_head_object()
    test_list_bucket_analytics_configurations()
    test_list_bucket_inventory_configurations()
    test_list_bucket_metrics_configurations()
    test_list_multipart_uploads()
    test_list_object_versions()
    test_list_objects()
    test_list_objects_v2()
    test_put_bucket_accelerate_configuration()
    test_put_bucket_encryption()
    test_put_bucket_policy()
    test_put_bucket_request_payment()
    test_put_bucket_tagging()
    test_put_bucket_versioning()
    test_put_object()
    test_upload_file()
    test_upload_fileobj()


def test_copy():
    try:
        copy_source = {
            'Bucket': BUCKET_NAME,
            'Key': 'test.py'
        }
        response = s3.copy(copy_source, OTHER_BUCKET_NAME, "copy_test")
    except ClientError as e:
        if e.response["Error"]["Code"] == "403":
            print("PASS! > test_copy")
        else:
            print("FAILED! > test_copy")


def test_copy_object():
    try:
        response = s3.copy_object(
            Bucket=OTHER_BUCKET_NAME,
            CopySource=f"{BUCKET_NAME}/test.py",
            Key="test.py"
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_copy_object")
        else:
            print("FAILED! > test_copy_object")


def test_create_multipart_upload():
    try:
        response = s3.create_multipart_upload(
            Bucket=BUCKET_NAME,
            Key="test-key"
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_create_multipart_upload")
        else:
            print("FAILED! > test_create_multipart_upload")


def test_delete_bucket():
    try:
        response = s3.delete_bucket(Bucket=BUCKET_NAME)
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_delete_bucket")
        else:
            print("FAILED! > test_delete_bucket")


def test_delete_bucket_analytics_configuration():
    try:
        response = s3.delete_bucket_analytics_configuration(
            Bucket=BUCKET_NAME,
            Id="23947294872"
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_delete_bucket_analytics_configuration")
        else:
            print("FAILED! > test_delete_bucket_analytics_configuration")


def test_delete_bucket_cors():
    try:
        response = s3.delete_bucket_cors(Bucket=BUCKET_NAME)
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_delete_bucket_cors")
        else:
            print("FAILED! > test_delete_bucket_cors")


def test_delete_bucket_encryption():
    try:
        response = s3.delete_bucket_encryption(Bucket=BUCKET_NAME)
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_delete_bucket_encryption")
        else:
            print("FAILED! > test_delete_bucket_encryption")


def test_delete_bucket_inventory_configuration():
    try:
        response = s3.delete_bucket_inventory_configuration(
            Bucket=BUCKET_NAME,
            Id="2497239472"
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_delete_bucket_inventory_configuration")
        else:
            print("FAILED! > test_delete_bucket_inventory_configuration")


def test_delete_bucket_lifecycle():
    try:
        response = s3.delete_bucket_lifecycle(Bucket=BUCKET_NAME)
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_delete_bucket_lifecycle")
        else:
            print("FAILED! > test_delete_bucket_lifecycle")


def test_delete_bucket_policy():
    try:
        response = s3.delete_bucket_policy(Bucket=BUCKET_NAME)
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_delete_bucket_policy")
        else:
            print("FAILED! > test_delete_bucket_policy")


def test_delete_bucket_replication():
    try:
        response = s3.delete_bucket_replication(Bucket=BUCKET_NAME)
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_delete_bucket_replication")
        else:
            print("FAILED! > test_delete_bucket_replication")


def test_delete_bucket_tagging():
    try:
        response = s3.delete_bucket_tagging(Bucket=BUCKET_NAME)
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_delete_bucket_tagging")
        else:
            print("FAILED! > test_delete_bucket_tagging")


def test_delete_bucket_website():
    try:
        response = s3.delete_bucket_website(Bucket=BUCKET_NAME)
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_delete_bucket_website")
        else:
            print("FAILED! > test_delete_bucket_website")


def test_delete_object():
    try:
        response = s3.delete_object(
            Bucket=BUCKET_NAME,
            Key="test.py"
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_delete_object")
        else:
            print("FAILED! > test_delete_object")


def test_delete_object_tagging():
    try:
        response = s3.delete_object_tagging(
            Bucket=BUCKET_NAME,
            Key="test.py"
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_delete_object_tagging")
        else:
            print("FAILED! > test_delete_object_tagging")


def test_delete_objects():
    response = s3.delete_objects(
        Bucket=BUCKET_NAME,
        Delete={
            "Objects": [
                {
                    "Key": "test.py"
                }
            ]
        }
    )
    if response["Errors"][0]["Code"] == "AccessDenied":
        print("PASS! > test_delete_objects")
    else:
        print("FAILED! > test_delete_objects")


def test_delete_public_access_block():
    try:
        response = s3.delete_public_access_block(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_delete_public_access_block")
        else:
            print("FAILED! > test_delete_public_access_block")


def test_download_file():
    try:
        response = s3.download_file(
            Bucket=BUCKET_NAME,
            Key="test.py",
            Filename="."
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "403":
            print("PASS! > test_download_file")
        else:
            print("FAILED! > test_download_file")


def test_download_fileobj():
    try:
        with open('filename', 'wb') as data:
            response = s3.download_fileobj(BUCKET_NAME, 'test.py', data)
    except ClientError as e:
        if e.response["Error"]["Code"] == "403":
            print("PASS! > test_download_fileobj")
        else:
            print("FAILED! > test_download_fileobj")


def test_get_bucket_accelerate_configuration():
    try:
        response = s3.get_bucket_accelerate_configuration(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_get_bucket_accelerate_configuration")
        else:
            print("FAILED! > test_get_bucket_accelerate_configuration")


def test_get_bucket_acl():
    try:
        response = s3.get_bucket_acl(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_get_bucket_acl")
        else:
            print("FAILED! > test_get_bucket_acl")


def test_get_bucket_cors():
    try:
        response = s3.get_bucket_cors(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_get_bucket_cors")
        else:
            print("FAILED! > test_get_bucket_cors")


def test_get_bucket_encryption():
    try:
        response = s3.get_bucket_encryption(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_get_bucket_encryption")
        else:
            print("FAILED! > test_get_bucket_encryption")


def test_get_bucket_inventory_configuration():
    try:
        response = s3.get_bucket_inventory_configuration(
            Bucket=BUCKET_NAME,
            Id="02040404028024820"
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_get_bucket_inventory_configuration")
        else:
            print("FAILED! > test_get_bucket_inventory_configuration")


def test_get_bucket_lifecycle():
    try:
        response = s3.get_bucket_lifecycle(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_get_bucket_lifecycle")
        else:
            print("FAILED! > test_get_bucket_lifecycle")


def test_get_bucket_lifecycle_configuration():
    try:
        response = s3.get_bucket_lifecycle_configuration(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_get_bucket_lifecycle_configuration")
        else:
            print("FAILED! > test_get_bucket_lifecycle_configuration")


def test_get_bucket_location():
    try:
        response = s3.get_bucket_location(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_get_bucket_location")
        else:
            print("FAILED! > test_get_bucket_location")


def test_get_bucket_logging():
    try:
        response = s3.get_bucket_logging(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_get_bucket_logging")
        else:
            print("FAILED! > test_get_bucket_logging")


def test_get_bucket_metrics_configuration():
    try:
        response = s3.get_bucket_metrics_configuration(
            Bucket=BUCKET_NAME,
            Id="2342532"
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_get_bucket_metrics_configuration")
        else:
            print("FAILED! > test_get_bucket_metrics_configuration")


def test_get_bucket_notification():
    try:
        response = s3.get_bucket_notification(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_get_bucket_notification")
        else:
            print("FAILED! > test_get_bucket_notification")


def test_get_bucket_notification_configuration():
    try:
        response = s3.get_bucket_notification_configuration(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_get_bucket_notification_configuration")
        else:
            print("FAILED! > test_get_bucket_notification_configuration")


def test_get_bucket_policy():
    try:
        response = s3.get_bucket_policy(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_get_bucket_policy")
        else:
            print("FAILED! > test_get_bucket_policy")


def test_get_bucket_policy_status():
    try:
        response = s3.get_bucket_policy_status(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_get_bucket_policy_status")
        else:
            print("FAILED! > test_get_bucket_policy_status")


def test_get_bucket_replication():
    try:
        response = s3.get_bucket_replication(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_get_bucket_replication")
        else:
            print("FAILED! > test_get_bucket_replication")


def test_get_bucket_request_payment():
    try:
        response = s3.get_bucket_request_payment(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_get_bucket_request_payment")
        else:
            print("FAILED! > test_get_bucket_request_payment")


def test_get_bucket_tagging():
    try:
        response = s3.get_bucket_tagging(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_get_bucket_tagging")
        else:
            print("FAILED! > test_get_bucket_tagging")


def test_get_bucket_versioning():
    try:
        response = s3.get_bucket_versioning(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_get_bucket_versioning")
        else:
            print("FAILED! > test_get_bucket_versioning")


def test_get_bucket_website():
    try:
        response = s3.get_bucket_website(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_get_bucket_website")
        else:
            print("FAILED! > test_get_bucket_website")


def test_get_object():
    try:
        response = s3.get_object(
            Bucket=BUCKET_NAME,
            Key="test.py"
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_get_object")
        else:
            print("FAILED! > test_get_object")


def test_get_object_acl():
    try:
        response = s3.get_object_acl(
            Bucket=BUCKET_NAME,
            Key="test.py"
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_get_object_acl")
        else:
            print("FAILED! > test_get_object_acl")


def test_get_object_lock_configuration():
    try:
        response = s3.get_object_lock_configuration(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_get_object_lock_configuration")
        else:
            print("FAILED! > test_get_object_lock_configuration")


def test_get_object_tagging():
    try:
        response = s3.get_object_tagging(
            Bucket=BUCKET_NAME,
            Key="test.py"
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_get_object_tagging")
        else:
            print(e)
            print("FAILED! > test_get_object_tagging")


def test_get_object_torrent():
    try:
        response = s3.get_object_torrent(
            Bucket=BUCKET_NAME,
            Key="test.py"
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_get_object_torrent")
        else:
            print(e)
            print("FAILED! > test_get_object_torrent")


def test_get_public_access_block():
    try:
        response = s3.get_public_access_block(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_get_public_access_block")
        else:
            print(e)
            print("FAILED! > test_get_public_access_block")


def test_head_bucket():
    try:
        response = s3.head_bucket(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "403":
            print("PASS! > test_head_bucket")
        else:
            print("FAILED! > test_head_bucket")


def test_head_object():
    try:
        response = s3.head_object(
            Bucket=BUCKET_NAME,
            Key="test.py"
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "403":
            print("PASS! > test_head_object")
        else:
            print("FAILED! > test_head_object")


def test_list_bucket_analytics_configurations():
    try:
        response = s3.list_bucket_analytics_configurations(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_list_bucket_analytics_configurations")
        else:
            print("FAILED! > test_list_bucket_analytics_configurations")


def test_list_bucket_inventory_configurations():
    try:
        response = s3.list_bucket_inventory_configurations(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_list_bucket_inventory_configurations")
        else:
            print("FAILED! > test_list_bucket_inventory_configurations")


def test_list_bucket_metrics_configurations():
    try:
        response = s3.list_bucket_metrics_configurations(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_list_bucket_metrics_configurations")
        else:
            print("FAILED! > test_list_bucket_metrics_configurations")


def test_list_multipart_uploads():
    try:
        response = s3.list_multipart_uploads(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_list_multipart_uploads")
        else:
            print("FAILED! > test_list_multipart_uploads")


def test_list_object_versions():
    try:
        response = s3.list_object_versions(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_list_object_versions")
        else:
            print("FAILED! > test_list_object_versions")


def test_list_objects():
    try:
        response = s3.list_objects(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_list_objects")
        else:
            print("FAILED! > test_list_objects")


def test_list_objects_v2():
    try:
        response = s3.list_objects_v2(
            Bucket=BUCKET_NAME
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_list_objects_v2")
        else:
            print("FAILED! > test_list_objects_v2")


def test_put_bucket_accelerate_configuration():
    try:
        response = s3.put_bucket_accelerate_configuration(
            Bucket=BUCKET_NAME,
            AccelerateConfiguration={
                "Status": "Enabled"
            }
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_put_bucket_accelerate_configuration")
        else:
            print("FAILED! > test_put_bucket_accelerate_configuration")


def test_put_bucket_encryption():
    try:
        response = s3.put_bucket_encryption(
            Bucket=BUCKET_NAME,
            ServerSideEncryptionConfiguration={
                'Rules': [
                    {
                        'ApplyServerSideEncryptionByDefault': {
                            'SSEAlgorithm': 'AES256'
                        }
                    },
                ]
            }
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_put_bucket_encryption")
        else:
            print("FAILED! > test_put_bucket_encryption")


def test_put_bucket_policy():
    policy = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Deny",
                "Principal": "*",
                "Action": "s3:*",
                "Resource": [
                    "arn:aws:s3:::testing-bukkit",
                    "arn:aws:s3:::testing-bukkit/*"
                ],
                "Condition": {
                    "StringNotEquals": {
                        "aws:PrincipalTag/application_id": "testid"
                    }
                }
            }
        ]
    }
    policy_json = json.dumps(policy)
    try:
        response = s3.put_bucket_policy(
            Bucket=BUCKET_NAME,
            Policy=policy_json
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_put_bucket_policy")
        else:
            print("FAILED! > test_put_bucket_policy")


def test_put_bucket_request_payment():
    try:
        response = s3.put_bucket_request_payment(
            Bucket=BUCKET_NAME,
            RequestPaymentConfiguration={
                'Payer': 'BucketOwner'
            }
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_put_bucket_request_payment")
        else:
            print("FAILED! > test_put_bucket_request_payment")


def test_put_bucket_tagging():
    try:
        response = s3.put_bucket_tagging(
            Bucket=BUCKET_NAME,
            Tagging={
                'TagSet': [
                    {
                        'Key': 'testKey',
                        'Value': 'testValue'
                    },
                ]
            }
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_put_bucket_tagging")
        else:
            print("FAILED! > test_put_bucket_tagging")


def test_put_bucket_versioning():
    try:
        response = s3.put_bucket_versioning(
            Bucket=BUCKET_NAME,
            VersioningConfiguration={
                'MFADelete': 'Enabled',
                'Status': 'Enabled'
            }
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_put_bucket_versioning")
        else:
            print("FAILED! > test_put_bucket_versioning")


def test_put_object():
    try:
        response = s3.put_object(
            Bucket=BUCKET_NAME,
            Key="testing"
        )
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_put_object")
        else:
            print("FAILED! > test_put_object")


def test_upload_file():
    try:
        response = s3.upload_file(
            Bucket=BUCKET_NAME,
            Key="test",
            Filename="testfile"
        )
    except Exception as e:
        error = "Failed to upload testfile to testing-bukkit/test: An error occurred (AccessDenied) when calling the PutObject operation: Access Denied"
        if str(e) == error:
            print("PASS! > test_upload_file")
        else:
            print("FAILED! > test_upload_file")


def test_upload_fileobj():
    try:
        with open('filename', 'rb') as data:
            s3.upload_fileobj(data, BUCKET_NAME, 'mykey')
    except ClientError as e:
        if e.response["Error"]["Code"] == "AccessDenied":
            print("PASS! > test_upload_fileobj")
        else:
            print("FAILED! > test_upload_fileobj")


if __name__ == "__main__":
    main()
